<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="format-detection" content="telephone=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<?php include('meta.php'); ?>
	<link href="/assets/css/common.css" rel="stylesheet">
	<link href="/assets/css/style.css" rel="stylesheet">
	<script src="/assets/js/jquery-3.3.1.min.js"></script>
	<script src="/assets/js/jquery-migrate-3.0.1.min.js"></script>
	<!-- <script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js"></script> -->
	<script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js?autoload=true&amp;skin=sunburst&amp;lang=css" defer=""></script>
	<script src="/assets/js/functions.js"></script>

	<script>
		$(function() {

			////////////////////////////////////////////////////////////
			// MENU
			////////////////////////////////////////////////////////////
			$(window).menu({
				nemu: '.c-menu',
				durationHoverItem: 300,
				nemuItem: '.c-menu__item',
				subMenu1: '.c-submenu, .c-submenu2',
				nemuItemSubMenu: '.c-submenu2__item',
				subMenu2: '.c-submenu3',
				menuOpen: '.c-header__button',
				slideInnerSP: '.c-header__menu',
				durationSlideSP: 500,
				arrow: '.icon',
				durationHoverLink: 300,
				hoverBackgroundColor: '#006689',
				hoverTextColor: '#fff',
				breakpoint: 767
			});

			////////////////////////////////////////////////////////////
			// SEACH
			////////////////////////////////////////////////////////////
			$('#search-btn-js').search({
				duration: 300,
				hide: '#search-view-js',
				breakpoint: 767
			});

			////////////////////////////////////////////////////////////
			// CART
			////////////////////////////////////////////////////////////

			//PC
			$('#cart-btn-js').cart({
				duration: 300,
				hide: '#cart-view-js'
			});

			// SP
			$('#cartsp-btn-js').cartsp({
				duration: 300,
				hide: '#cartsp-view-js'
			});

		});
	</script>
</head>
<body class="page-<?php echo $pageid; ?>">

	<header class="c-header">
		<div class="c-header__inner">
			<div class="c-header__inner2 pc-only">
				<div class="c-header__wrap">
					<div class="c-header__txt">
						<p>高級アジアン家具・リゾート家具・ウォーターヒアシンス家具・ラタン家具専門通販</p>
					</div>
					<ul class="c-header__cart">
						<li>
							<a href="#">会員登録</a>
						</li>
						<li>
							<a href="#">ログイン</a>
						</li>
						<li class="cart" id="cart-btn-js">
							<a href="#">カート</a>
							<div class="c-header__cart__box" id="cart-view-js">
								<p>只今、カートに商品はございません。</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="c-header__inner3">
				<div class="c-header__wrap">
					<div class="c-header__logo">
						<a href="/">
							<img src="/assets/img/common/logo_le.png" width="129" height="50" alt="">
						</a>
					</div>
					<div class="c-header__menu">
						<nav class="c-header__navi">
							<ul class="c-menu">
								<li class="c-menu__item">
									<a class="c-menu__link" href="#">シーンで選ぶ<span class="icon"><i></i></span></a>
									<nav class="c-submenu">
										<ul>
											<li>
												<a href="#">
													<div class="c-submenu__image pc-only">
														<img src="/assets/img/common/living-2-740x460.jpg">
													</div>
													<div class="c-submenu__text">
														<p>リビング</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="c-submenu__image pc-only">
														<img src="/assets/img/common/dining-740x460.jpg">
													</div>
													<div class="c-submenu__text">
														<p>ダイニング</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="c-submenu__image pc-only">
														<img src="/assets/img/common/bed-740x460.jpg">					
													</div>
													<div class="c-submenu__text">
														<p>ベッドルーム</p>
													</div>
												</a>
											</li>
										</ul>
									</nav>
								</li>
								<li class="c-menu__item">
									<a class="c-menu__link" href="#">カテゴリーから選ぶ<span class="icon"><i></i></span></a>
									<nav class="c-submenu">
										<ul>
											<li>
												<a href="#">
													<div class="c-submenu__image pc-only">
														<img src="/assets/img/common/table-740x460.jpg">
													</div>
													<div class="c-submenu__text">
														<p>テーブル</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="c-submenu__image pc-only">
														<img src="/assets/img/common/chair-740x460.jpg">
													</div>
													<div class="c-submenu__text">
														<p>チェアー</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="c-submenu__image pc-only">
														<img src="/assets/img/common/sofa-740x460.jpg">
													</div>
													<div class="c-submenu__text">
														<p>ソファ</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="c-submenu__image pc-only">
														<img src="/assets/img/common/bed-2-740x460.jpg">
													</div>
													<div class="c-submenu__text">
														<p>ベッド</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="c-submenu__image pc-only">
														<img src="/assets/img/common/ele-740x460.jpg">
													</div>
													<div class="c-submenu__text">
														<p>ランプ・鏡・雑貨</p>
													</div>
												</a>
											</li>
										</ul>
									</nav>
								</li>
								<li class="c-menu__item">
									<a class="c-menu__link" href="#">素材から選ぶ<span class="icon"><i></i></span></a>
									<nav class="c-submenu">
										<ul>
											<li>
												<a href="#">
													<div class="c-submenu__image pc-only">
														<img src="/assets/img/common/w-yacinth-740x460.jpg">
													</div>
													<div class="c-submenu__text">
														<p>ウォーターヒアシンス家具</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="c-submenu__image pc-only">
														<img src="/assets/img/common/ratthan-740x460.jpg">
													</div>
													<div class="c-submenu__text">
														<p>ラタン家具</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="c-submenu__image pc-only">
														<img src="/assets/img/common/bamboo-740x460.jpg">
													</div>
													<div class="c-submenu__text">
														<p>バンブー家具</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="c-submenu__image pc-only">
														<img src="/assets/img/common/rether-740x460.jpg">
													</div>
													<div class="c-submenu__text">
														<p>レザー家具</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#">
													<div class="c-submenu__image pc-only">
														<img src="/assets/img/common/wood-1-740x460.jpg">
													</div>
													<div class="c-submenu__text">
														<p>ウッド家具</p>
													</div>
												</a>
											</li>
										</ul>
									</nav>
								</li>
								<li class="c-menu__item">
									<a class="c-menu__link" href="#">導入事例</a>
								</li>
								<li class="c-menu__item">
									<a class="c-menu__link" href="#">サポート<span class="icon"><i></i></span></a>
									<ul class="c-submenu2">
										<li class="c-submenu2__item">
											<a class="c-submenu2__link" href="#">ご利用案内<span class="icon"><i></i></span></a>
											<ul class="c-submenu3">
												<li><a href="#">ご購入方法</a></li>
												<li><a href="#">お届けまでの流れ</a></li>
												<li><a href="#">会社情報</a></li>
											</ul>
										</li>
										<li class="c-submenu2__item">
											<a class="c-submenu2__link" href="#">製品情報</a>
										</li>
										<li class="c-submenu2__item">
											<a class="c-submenu2__link" href="#">FAQ<span class="icon"><i></i></span></a>
											<ul class="c-submenu3">
												<li><a href="#">商品に関する質問</a></li>
												<li><a href="#">セミオーダーに関する質問</a></li>
												<li><a href="#">購入に関する質問</a></li>
												<li><a href="#">配送・設置に関する質問</a></li>
												<li><a href="#">メンテナンスに関する質問</a></li>
												<li><a href="#">修理に関する質問</a></li>
											</ul>
										</li>
										<li class="c-submenu2__item">
											<a class="c-submenu2__link" href="#">お知らせ</a>
										</li>
									</ul>
								</li>
								<li class="c-menu__item">
									<a class="c-menu__link" href="#">お問合せ</a>
								</li>
								<li class="c-menu__item sp-only">
									<a class="c-menu__link" href="#">会員登録</a>
								</li>
								<li class="c-menu__item sp-only">
									<a class="c-menu__link" href="#">ログイン</a>
								</li>
							</ul>
						</nav>
						
						<div class="c-header__search">
							<a class="c-header__searchIcon" id="search-btn-js">S</a>
							<div class="c-header__searchForm" id="search-view-js">
								<form action="" method="get">
									<input type="text" name="s" value="" placeholder="SEARCH">
									<input type="submit" value="S" class="sp-only">
								</form>
							</div>
						</div>
					</div>
					<div class="c-header__cartsp sp-only" id="cartsp-btn-js">
						<a href="#">0</a>
					</div>
					<div class="c-header__cartspBox sp-only" id="cartsp-view-js">
						<p>只今、カートに商品はございません。</p>
					</div>
					<div class="c-header__button sp-only">
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>
			</div>
			<div class="c-header__inner2 sp-only">
				<div class="c-header__wrap">
					<div class="c-header__txt">
						<p>高級アジアン家具・リゾート家具・ウォーターヒアシンス家具・ラタン家具専門通販</p>
					</div>
				</div>
			</div>
		</div>
	</header>