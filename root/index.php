<?php $pageid="index";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<main id="main" role="main">
	<div id="conts">

		<section class="section_conts conts_inner mt0">
			<h2 class="heading01">Menu</h2>
			<p>Đây là một plugin menu.</p>

			<section class="section_conts">
				<h3 class="heading02">Cách sử dụng</h3>
				<p>1. Vui lòng tải function.js</p>
				<p>2. Vui lòng tải style.css</p>
				<p>3. Vui lòng tham khảo ví dụ sau đây.</p>
				<pre class="prettyprint" id="quine"></pre>
				<div>
					<img src="/assets/img/instructions/01.jpg">
				</div>
				<p>4. Vui lòng sửa dụng:</p>
				<pre class="prettyprint lang-js">
					$(window).menu({
					nemu: '.c-menu',
					durationHoverItem: 300,
					nemuItem: '.c-menu__item',
					subMenu1: '.c-submenu, .c-submenu2',
					nemuItemSubMenu: '.c-submenu2__item',
					subMenu2: '.c-submenu3',
					menuOpen: '.c-header__button',
					slideInnerSP: '.c-header__menu',
					durationSlideSP: 500,
					arrow: '.icon',
					durationHoverLink: 300,
					hoverBackgroundColor: '#006689',
					hoverTextColor: '#fff',
					breakpoint: 767
				});
			</pre>
			<p>5. Thay đổi hành vi với các tùy chọn khi cần thiết.</p>
		</section>

		<section class="section_conts">
			<h3 class="heading02">Tùy chọn</h3>
			<div class="tblWrapper">
				<table>
					<tr>
						<th>Tên tùy chọn</th>
						<th>Cài đặt</th>
						<th>Định dạng</th>
						<th>Chi tiết</th>
					</tr>
					<tr>
						<td>nemu</td>
						<td>Menu điều hướng toàn cầu</td>
						<td>string</td>
						<td>Tên class menu điều hướng toàn cầu</td>
					</tr>
					<tr>
						<td>durationHoverItem</td>
						<td>Tốc độ hiển thị menu con khi hover</td>
						<td>string</td>
						<td>Chỉ định tốc độ hiện thị và mờ dần tính bằng mili giây</td>
					</tr>
					<tr>
						<td>nemuItem</td>
						<td>Item menu điều hướng toàn cầu</td>
						<td>string</td>
						<td>Tên class item menu điều hướng toàn cầu</td>
					</tr>
					<tr>
						<td>subMenu1</td>
						<td>Menu con của item menu điều hướng toàn cầu</td>
						<td>string</td>
						<td>Tên class menu con trong item nemu điều hướng toàn cầu</td>
					</tr>
					<tr>
						<td>nemuItemSubMenu</td>
						<td>Item menu của menu con có thêm 1 menu con bên trong</td>
						<td>string</td>
						<td>Tên class item menu con có thêm 1 menu con bên trong</td>
					</tr>
					<tr>
						<td>subMenu2</td>
						<td>Menu con của item menu con</td>
						<td>string</td>
						<td>Tên class menu con của item menu con</td>
					</tr>
					<tr>
						<td>menuOpen</td>
						<td>Nút menu SP</td>
						<td>string</td>
						<td>Tên class nút menu SP</td>
					</tr>
					<tr>
						<td>slideInnerSP</td>
						<td>Menu SP</td>
						<td>string</td>
						<td>Tên class menu SP</td>
					</tr>
					<tr>
						<td>durationSlideSP</td>
						<td>Tốc độ đóng - mở menu SP</td>
						<td>int</td>
						<td>Chỉ định tốc độ tính bằng mili giây</td>
					</tr>
					<tr>
						<td>arrow</td>
						<td>Mũi tên</td>
						<td>string</td>
						<td>Tên class mũi tên</td>
					</tr>
					<tr>
						<td>durationHoverLink</td>
						<td>Tốc độ chuyển đổi khi hover</td>
						<td>int</td>
						<td>Chỉ định tốc độ tính bằng mili giây</td>
					</tr>
					<tr>
						<td>hoverBackgroundColor</td>
						<td>Màu background khi hover liên kết</td>
						<td>string</td>
						<td>Chỉ định mã màu background khi hover liên kết</td>
					</tr>
					<tr>
						<td>hoverTextColor</td>
						<td>Màu text khi hover liên kết</td>
						<td>string</td>
						<td>Chỉ định mã màu text khi hover liên kết</td>
					</tr>
					<tr>
						<td>breakpoint</td>
						<td>Điểm dừng</td>
						<td>int</td>
						<td>Chỉ định chiều rộng của cửa sổ để chuyển đổi giữa PC và SP.</td>
					</tr>
				</table>
			</div>
		</section>

		<section class="section_conts">
			<h3 class="heading02">Đặc điểm kỹ thuật</h3>
			<p>PC: Mở - đóng menu bằng cách hover chuột.</p>
			<p>SP: Mở - đóng menu bằng cách click.</p>
		</section>

		<section class="section_conts">
			<h3 class="heading02">Phòng ngừa</h3>
			<p>...</p>
		</section>

		<section class="section_conts">
			<h3 class="heading02">Chức năng khác (Cart - Search)</h3>
			<h4>Cart (PC)</h4>
			<p>1. Vui lòng tham khảo ví dụ sau đây.</p>
			<pre class="prettyprint" id="quine2"></pre>
			<br>
			<p>2. Vui lòng sửa dụng:</p>
			<pre class="prettyprint lang-js">
				$('#cart-btn-js').cart({
				duration: 300,
				hide: '#cart-view-js'
			});
		</pre>
		<br><br>

		<h4>Cart (SP)</h4>
		<p>1. Vui lòng tham khảo ví dụ sau đây.</p>
		<pre class="prettyprint" id="quine3"></pre>
		<br>
		<p>2. Vui lòng sửa dụng:</p>
		<pre class="prettyprint lang-js">
			$('#cartsp-btn-js').cartsp({
			duration: 300,
			hide: '#cartsp-view-js'
		});
	</pre>
	<br><br>

	<h4>Search (PC)</h4>
	<p>1. Vui lòng tham khảo ví dụ sau đây.</p>
	<pre class="prettyprint" id="quine4"></pre>
	<br>
	<p>2. Vui lòng sửa dụng:</p>
	<pre class="prettyprint lang-js">
		$('#search-btn-js').search({
		duration: 300,
		hide: '#search-view-js',
		breakpoint: 767
	});
</pre>
</section>

<section class="section_conts">
	<h3 class="heading02">Tùy chọn</h3>
	<div class="tblWrapper">
		<table>
			<tr>
				<th>Tên tùy chọn</th>
				<th>Cài đặt</th>
				<th>Định dạng</th>
				<th>Chi tiết</th>
			</tr>
			<tr>
				<td>duration</td>
				<td>Tốc độ hiển thị</td>
				<td>int</td>
				<td>Chỉ định tốc độ tính bằng mili giây</td>
			</tr>
			<tr>
				<td>hide</td>
				<td>Phần tử bị ẩn</td>
				<td>string</td>
				<td>Tên id phần tử cần ẩn</td>
			</tr>
			<tr>
				<td>breakpoint</td>
				<td>Điểm dừng</td>
				<td>int</td>
				<td>Chỉ định chiều rộng của cửa sổ để chuyển đổi giữa PC và SP.</td>
			</tr>
		</table>
	</div>
</section>

<section class="section_conts">
	<h3 class="heading02">Phòng ngừa</h3>
	<p>...</p>
</section>
</section>
</div>
<!-- /#conts -->
</main>
<!-- /#main -->


<script type="text/javascript">
	(function () {
		function htmlEscape(s) {
			return s
			.replace(/&/g, '&amp;')
			.replace(/</g, '&lt;')
			.replace(/>/g, '&gt;');
		}
		var quineHtml = htmlEscape(
			'<header class="c-header">' +
			$(".c-header").html() +
			'</header>');

		var quineHtml2 = htmlEscape(
			'<ul class="c-header__cart">' +
			$(".c-header__cart").html() +
			'</ul>');

		var quineHtml3 = htmlEscape(
			'<div class="c-header__cartsp sp-only" id="cartsp-btn-js">\n<a href="#">0</a>\n</div>\n<div class="c-header__cartspBox sp-only" id="cartsp-view-js">\n<p>ートに商品はございません。</p>\n</div>');
		
		var quineHtml4 = htmlEscape(
			'<div class="c-header__search">' +
			$(".c-header__search").html() +
			'</div>');

		

// Highlight the operative parts:
quineHtml = quineHtml.replace(
	/&lt;script src[\s\S]*?&gt;&lt;\/script&gt;|&lt;!--\?[\s\S]*?--&gt;|&lt;pre\b[\s\S]*?&lt;\/pre&gt;/g,
	'<span class="operative">$&<\/span>');

quineHtml2 = quineHtml2.replace(
	/&lt;script src[\s\S]*?&gt;&lt;\/script&gt;|&lt;!--\?[\s\S]*?--&gt;|&lt;pre\b[\s\S]*?&lt;\/pre&gt;/g,
	'<span class="operative">$&<\/span>');

quineHtml3 = quineHtml3.replace(
	/&lt;script src[\s\S]*?&gt;&lt;\/script&gt;|&lt;!--\?[\s\S]*?--&gt;|&lt;pre\b[\s\S]*?&lt;\/pre&gt;/g,
	'<span class="operative">$&<\/span>');

// insert into PRE
document.getElementById("quine").innerHTML = quineHtml;
document.getElementById("quine2").innerHTML = quineHtml2;
document.getElementById("quine3").innerHTML = quineHtml3;
document.getElementById("quine4").innerHTML = quineHtml4;
})();
</script>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
