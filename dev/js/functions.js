(function($){

/* ======================================
menu
====================================== */

$.fn.menu = function(options) {
	var opts = $.extend({}, $.fn.menu.defaults, options);
	var init = function() {
		initializeMenu();
		initializeSubMenu();
		initializeMenuSP();
		$(window).on('resize', function() {
			initializeMenu();
			initializeSubMenu();
			initializeMenuSP();
		});
	}();
	function is_sp() {
		var win_w = $(window).width();
		return opts.breakpoint > win_w ? true : false;
	}

	// initialize
	function initializeMenu() {
		if( is_sp()) {
			$(opts.subMenu1).removeAttr('style');
		} else {
			$(opts.subMenu1).css({
				opacity: 0,
				visibility: 'hidden',
				transform: 'translate3d(0, -5px, 0)',
				'transition-property': 'transform, opacity, visibility',
				'transition-duration': opts.durationHoverItem+'ms',
				'transition-timing-function': 'ease',
				'z-index' : -1
			});
		}
	}
	function initializeSubMenu() {
		if( is_sp()) {
			$(opts.subMenu2).removeAttr('style');
		} else {
			$(opts.subMenu2).css({
				opacity: 0,
				visibility: 'hidden',
				transform: 'translate3d(0, -5px, 0)',
				'transition-property': 'transform, opacity, visibility',
				'transition-duration': opts.durationHoverItem+'ms',
				'transition-timing-function': 'ease',
				'z-index' : -1
			});
		}
	}
	function initializeMenuSP() {
		if( is_sp() ) {
			$(opts.slideInnerSP).hide();
			$(opts.subMenu1).hide();
			$(opts.subMenu2).hide();
			$(opts.arrow + '> *').css({
				transition: opts.durationHoverLink+'ms ease-in-out',
			});
		} else {
			$(opts.menuOpen).removeClass('is-active');
			$(opts.slideInnerSP).removeAttr('style');
			$(opts.subMenu1).show();
			$(opts.subMenu2).show();
			$(opts.arrow).removeClass('is-active');
			$(opts.arrow + '> *').removeAttr('style');
		}
	}

	// function
	$(opts.nemuItem).hover(function() {
		if( !is_sp() ){
			$(this).find(opts.subMenu1).css({
				opacity: 1,
				visibility: 'visible',
				transform: 'translate3d(0, 0, 0)',
				'z-index' : 999
			});
		}
	}, function() {
		if( !is_sp() ){
			initializeMenu();
		}
	});
	$(opts.nemuItemSubMenu).hover(function() {
		if( !is_sp() ){
			$(this).find(opts.subMenu2).css({
				opacity: 1,
				visibility: 'visible',
				transform: 'translate3d(0, 0, 0)',
				'z-index' : 999
			});
		}
	}, function() {
		if( !is_sp() ){
			initializeSubMenu();
		}
	});
	$(opts.menuOpen).click(function() {
		if( is_sp()) {
			if( $(opts.slideInnerSP).is(':animated') ) return;
			$(this).toggleClass('is-active');
			$(opts.slideInnerSP).slideToggle(opts.durationSlideSP);
		}
	});
	$(opts.arrow).click(function(event) {
		if(is_sp()) {
			event.preventDefault();
			if( $(opts.subMenu1).is(':animated') || $(opts.subMenu2).is(':animated') ) return;
			$(this).toggleClass('is-active');
			$(this).parent().siblings(opts.subMenu1).slideToggle(opts.durationSlideSP);
			$(this).parent().siblings(opts.subMenu2).slideToggle(opts.durationSlideSP);
		}
	});
	$(opts.nemu + ' a').hover(function() {
		if($(this).parent().is(opts.nemuItem) && !is_sp()) {
			//
		} else {
			$(this).css({
				transition: opts.durationHoverLink+'ms ease-in-out',
				background: opts.hoverBackgroundColor,
				color: opts.hoverTextColor
			});
			$(this).children().css({
				transition: opts.durationHoverLink+'ms ease-in-out',
				color: opts.hoverTextColor
			});
			$(this).find(opts.arrow+ ' >*').css({
				transition: opts.durationHoverLink+'ms ease-in-out',
				'border-color': opts.hoverTextColor
			});
		}
	}, function() {
		$(this).css({
			background: '',
			color: ''
		});
		$(this).children().css({
			color: ''
		});
		$(this).find(opts.arrow+ '>*').css({
			'border-color': ''
		});
	});
};

$.fn.menu.defaults = {
	nemu: '.c-menu',
	durationHoverItem: 300,
	nemuItem: '.c-menu__item',
	subMenu1: '.c-submenu, .c-submenu2',
	nemuItemSubMenu: '.c-submenu2__item',
	subMenu2: '.c-submenu3',
	menuOpen: '.c-header__button',
	slideInnerSP: '.c-header__menu',
	durationSlideSP: 500,
	arrow: '.icon',
	durationHoverLink: 300,
	hoverBackgroundColor: '#006689',
	hoverTextColor: '#fff',
	breakpoint: 767
};


/* ======================================
cart
====================================== */

//PC
$.fn.cart = function(options) {
	var opts = $.extend({}, $.fn.cart.defaults, options);
	var init = function() {
		initialize();
		$(window).on('resize', function() {
			initialize();
		});
	}();
	function initialize() {
		$(opts.hide).css({
			opacity: 0,
			visibility: 'hidden',
			transform: 'translate3d(0, -5px, 0)',
			'transition-property': 'transform, opacity, visibility',
			'transition-duration': opts.duration+'ms',
			'transition-timing-function': 'ease'
		});
	}
	$(this).hover(function() {
		$(opts.hide).css({
			opacity: 1,
			visibility: 'visible',
			transform: 'translate3d(0, 0, 0)'
		});
	}, function() {
		initialize();
	});
};
$.fn.cart.defaults = {
	duration: 300,
	hide: '#cart-view-js'
};

// SP
$.fn.cartsp = function(options) {
	var opts = $.extend({}, $.fn.cartsp.defaults, options);
	var init = function() {
		initialize();
		$(window).on('resize', function() {
			initialize();
		});
	}();
	function initialize() {
		$(opts.hide).css({
			opacity: 0,
			visibility: 'hidden',
			transform: 'translate3d(0, -5px, 0)',
			'transition-property': 'transform, opacity, visibility',
			'transition-duration': opts.duration+'ms',
			'transition-timing-function': 'ease'
		});
	}
	$(this).click(function() {
		$(opts.hide).toggleClass('is-active');
		if($(opts.hide).hasClass('is-active')) {
			$(opts.hide).css({
				opacity: 1,
				visibility: 'visible',
				transform: 'translate3d(0, 0, 0)'
			});
		} else {
			initialize();
		}
	});
};
$.fn.cartsp.defaults = {
	duration: 300,
	hide: '#cartsp-view-js',
};


/* ======================================
search
====================================== */
$.fn.search = function(options) {
	var opts = $.extend({}, $.fn.search.defaults, options);
	var init = function() {
		initialize();
		$(window).on('resize', function() {
			initialize();
		});
	}();
	function is_sp() {
		var win_w = $(window).width();
		return opts.breakpoint > win_w ? true : false;
	}
	function initialize() {
		if(is_sp()) {
			$(opts.hide).removeAttr('style');
		} else {
			$(opts.hide).css({
				opacity: 0,
				visibility: 'hidden',
				'margin-top': '-30px',
				transition: 'all '+ opts.duration +'ms'
			});
		}
	}
	$(this).click(function() {
		if( !is_sp()) {
			$(opts.hide).toggleClass('is-active');
			if($(opts.hide).hasClass('is-active')) {
				$(opts.hide).css({
					opacity: 1,
					visibility: 'visible',
					'margin-top': '0'
				});
			} else {
				initialize();
			}
		}
	});

};
$.fn.search.defaults = {
	duration: 300,
	hide: '#search-view-js',
	breakpoint: 767
};

})(jQuery);